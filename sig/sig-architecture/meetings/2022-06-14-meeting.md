# 架构SIG例会 2022-6-14 9:30-11:30(UTC+08:00)Beijing

## 议题(Agenda):
议题1、sig仓申请建仓：device_board_starfive，device_soc_starfive，device_vendor_starfive<br>
议题2、sig仓申请建仓：third_party_elfutils<br>
议题3、规范部件仓名和目录申请：multimedia_medialibrary_standard，windowmanager<br>
议题4、iot_link、驱动子系统部件仓和目录优化<br>

## 与会人(Attendees)： 
任革林 [@im-off-this-week](https://gitee.com/im-off-this-week)<br>
董金光 [@dongjinguang](https://gitee.com/dongjinguang)<br>
赵文华 [@shidi_snow](https://gitee.com/shidi_snow)<br>
万承臻 [@wanchengzhen](https://gitee.com/wanchengzhen)<br>
强波   [@huawei_qiangbo](https://gitee.com/huawei_qiangbo)<br>
黄明龙 [@minglonghuang](https://gitee.com/minglonghuang)<br>
梁克雷 [@xzmu](https://gitee.com/xzmu)<br>

## 会议纪要(Notes)
**议题1、sig仓申请建仓：device_board_starfive，device_soc_starfive，device_vendor_starfive**<br>
汇报人：娄山林<br>
会议结论：<br>
1、暂不同意新建。开发板相关内容待RISC-V SIG的工作对接OpenHarmony社区后再孵化。<br>
遗留问题：<br>
1、择期在PMC例会汇报RISC-V SIG整体工作内容、方案及其进展。责任人：RISC-V SIG。<br>

**议题2、sig仓申请建仓：third_party_elfutils**<br>
汇报人：毛思平<br>
会议结论：<br>
1、暂不同意新建。继续分析本系统如何使用该软件，使用其哪些部分，对应开源协议是什么，后续如何维护等情况后再汇报决策。<br>
遗留问题：<br>
无。<br>

**议题3、规范部件仓名和目录申请：multimedia_medialibrary_standard，windowmanager**<br>
汇报人：罗嘉豪、毛江平<br>
会议结论：<br>
暂不同意修改。<br>
1、multimedia_medialibrary_standard：媒体域从整体审视后统一整改，不单独修改；<br>
2、windowmanager：拉通轻量级设备窗口仓名统一修改。<br>
遗留问题：<br>
无。<br>

**议题4、iot_link、驱动子系统部件仓和目录优化**<br>
汇报人：裴太乙<br>
会议结论：<br>
同意iot_link和驱动相关的部件名、仓和路径的优化方案：<br>
1、将iot_link下的源码归并到sample_wifi_iot部件下，保留为三方link厂商提供快速集成的示例功能，iot_link仓停止维护，domain/iot/link目录删除；<br>
2、驱动drivers/framework和drivers/adapter目录合并一个部件hdf_core, drivers/peripheral和drivers/interface下的部件按外设划分23个部件，命名方式为“drivers_peripheral_<外设>“和”drivers_interface_<外设>”。<br>
遗留问题：<br>
无。<br>